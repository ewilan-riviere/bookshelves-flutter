// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GoogleBook.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GoogleBook _$GoogleBookFromJson(Map<String, dynamic> json) {
  return GoogleBook(
    previewLink: json['previewLink'] as String?,
    buyLink: json['buyLink'] as String?,
    retailPrice: json['retailPrice'] as int?,
    retailPriceCurrency: json['retailPriceCurrency'] as String?,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
  );
}

Map<String, dynamic> _$GoogleBookToJson(GoogleBook instance) =>
    <String, dynamic>{
      'previewLink': instance.previewLink,
      'buyLink': instance.buyLink,
      'retailPrice': instance.retailPrice,
      'retailPriceCurrency': instance.retailPriceCurrency,
      'createdAt': instance.createdAt?.toIso8601String(),
    };
