import 'package:json_annotation/json_annotation.dart';

part 'Publisher.g.dart';

@JsonSerializable()
class Publisher {
  Publisher({
    this.name,
    this.slug,
  });

  String? name;
  String? slug;

  factory Publisher.fromJson(Map<String, dynamic> json) =>
      _$PublisherFromJson(json);
  Map<String, dynamic> toJson() => _$PublisherToJson(this);
}
