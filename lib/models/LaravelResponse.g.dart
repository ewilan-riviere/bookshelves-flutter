// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LaravelResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LaravelResponse _$LaravelResponseFromJson(Map<String, dynamic> json) {
  return LaravelResponse(
    data: json['data'] as List<dynamic>?,
    links: json['links'] == null
        ? null
        : LaravelLinks.fromJson(json['links'] as Map<String, dynamic>),
    meta: json['meta'] == null
        ? null
        : LaravelMeta.fromJson(json['meta'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$LaravelResponseToJson(LaravelResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'links': instance.links,
      'meta': instance.meta,
    };

LaravelLinks _$LaravelLinksFromJson(Map<String, dynamic> json) {
  return LaravelLinks(
    first: json['first'] as String?,
    last: json['last'] as String?,
    prev: json['prev'] as String?,
    next: json['next'] as String?,
  );
}

Map<String, dynamic> _$LaravelLinksToJson(LaravelLinks instance) =>
    <String, dynamic>{
      'first': instance.first,
      'last': instance.last,
      'prev': instance.prev,
      'next': instance.next,
    };

LaravelMeta _$LaravelMetaFromJson(Map<String, dynamic> json) {
  return LaravelMeta(
    currentPage: json['current_page'] as int?,
    from: json['from'] as int?,
    lastPage: json['last_page'] as int?,
    links: (json['links'] as List<dynamic>?)
        ?.map((e) => LaravelMetaLink.fromJson(e as Map<String, dynamic>))
        .toList(),
    path: json['path'] as String?,
    perPage: json['per_page'] as int?,
    to: json['to'] as int?,
    total: json['total'] as int?,
  );
}

Map<String, dynamic> _$LaravelMetaToJson(LaravelMeta instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'from': instance.from,
      'last_page': instance.lastPage,
      'links': instance.links,
      'path': instance.path,
      'per_page': instance.perPage,
      'to': instance.to,
      'total': instance.total,
    };

LaravelMetaLink _$LaravelMetaLinkFromJson(Map<String, dynamic> json) {
  return LaravelMetaLink(
    url: json['url'] as String?,
    label: json['label'] as String?,
    active: json['active'] as bool?,
  );
}

Map<String, dynamic> _$LaravelMetaLinkToJson(LaravelMetaLink instance) =>
    <String, dynamic>{
      'url': instance.url,
      'label': instance.label,
      'active': instance.active,
    };
