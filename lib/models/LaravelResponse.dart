import 'package:json_annotation/json_annotation.dart';

part 'LaravelResponse.g.dart';

@JsonSerializable()
class LaravelResponse {
  LaravelResponse({
    this.data,
    this.links,
    this.meta,
  });

  List<dynamic>? data; // Book, Serie, Author
  LaravelLinks? links;
  LaravelMeta? meta;

  factory LaravelResponse.fromJson(Map<String, dynamic> json) =>
      _$LaravelResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LaravelResponseToJson(this);
}

@JsonSerializable()
class LaravelLinks {
  LaravelLinks({
    this.first,
    this.last,
    this.prev,
    this.next,
  });

  String? first;
  String? last;
  String? prev;
  String? next;

  factory LaravelLinks.fromJson(Map<String, dynamic> json) =>
      _$LaravelLinksFromJson(json);
  Map<String, dynamic> toJson() => _$LaravelLinksToJson(this);
}

@JsonSerializable()
class LaravelMeta {
  LaravelMeta({
    this.currentPage,
    this.from,
    this.lastPage,
    this.links,
    this.path,
    this.perPage,
    this.to,
    this.total,
  });

  @JsonKey(name: 'current_page')
  int? currentPage;
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  List<LaravelMetaLink>? links;
  String? path;
  @JsonKey(name: 'per_page')
  int? perPage;
  int? to;
  int? total;

  factory LaravelMeta.fromJson(Map<String, dynamic> json) =>
      _$LaravelMetaFromJson(json);
  Map<String, dynamic> toJson() => _$LaravelMetaToJson(this);
}

@JsonSerializable()
class LaravelMetaLink {
  LaravelMetaLink({
    this.url,
    this.label,
    this.active,
  });

  String? url;
  String? label;
  bool? active;

  factory LaravelMetaLink.fromJson(Map<String, dynamic> json) =>
      _$LaravelMetaLinkFromJson(json);
  Map<String, dynamic> toJson() => _$LaravelMetaLinkToJson(this);
}
