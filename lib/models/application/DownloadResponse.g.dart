// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DownloadResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DownloadResponse _$DownloadResponseFromJson(Map<String, dynamic> json) {
  return DownloadResponse(
    success: json['success'] as bool,
    path: json['path'] as String?,
    filename: json['filename'] as String?,
  );
}

Map<String, dynamic> _$DownloadResponseToJson(DownloadResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'path': instance.path,
      'filename': instance.filename,
    };
