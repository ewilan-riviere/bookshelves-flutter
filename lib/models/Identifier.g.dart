// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Identifier.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Identifier _$IdentifierFromJson(Map<String, dynamic> json) {
  return Identifier(
    isbn: json['isbn'] as String?,
    isbn13: json['isbn13'] as String?,
    doi: json['doi'] as String?,
    amazon: json['amazon'] as String?,
    google: json['google'] as String?,
  );
}

Map<String, dynamic> _$IdentifierToJson(Identifier instance) =>
    <String, dynamic>{
      'isbn': instance.isbn,
      'isbn13': instance.isbn13,
      'doi': instance.doi,
      'amazon': instance.amazon,
      'google': instance.google,
    };
