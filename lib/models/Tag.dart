// To parse this JSON data, do
//
//     final tag = tagFromMap(jsonString);

import 'package:json_annotation/json_annotation.dart';

part 'Tag.g.dart';

@JsonSerializable()
class Tag {
  Tag({
    this.name,
    this.slug,
  });

  String? name;
  String? slug;

  factory Tag.fromJson(Map<String, dynamic> json) => _$TagFromJson(json);
  Map<String, dynamic> toJson() => _$TagToJson(this);
}
