import 'package:json_annotation/json_annotation.dart';

part 'Meta.g.dart';

@JsonSerializable()
class Meta {
  Meta({
    this.show,
  });

  String? show;

  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);
  Map<String, dynamic> toJson() => _$MetaToJson(this);
}
