// To parse this JSON data, do
//
//     final book = bookFromMap(jsonString);

import 'package:bookshelves/models/Author.dart';
import 'package:bookshelves/models/Epub.dart';
import 'package:bookshelves/models/GoogleBook.dart';
import 'package:bookshelves/models/Identifier.dart';
import 'package:bookshelves/models/Meta.dart';
import 'package:bookshelves/models/Picture.dart';
import 'package:bookshelves/models/Publisher.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:bookshelves/models/Tag.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Book.g.dart';

@JsonSerializable()
class Book {
  Book({
    this.title,
    this.slug,
    this.author,
    this.authors,
    this.summary,
    this.language,
    this.publishDate,
    this.picture,
    this.publisher,
    this.volume,
    this.meta,
    this.serie,
    this.description,
    this.identifier,
    this.pageCount,
    this.maturityRating,
    this.tags,
    this.epub,
    this.googleBook,
    this.isFavorite,
    // this.comments = '',
  });

  String? title;
  String? slug;
  String? author;
  List<Author>? authors;
  String? summary;
  String? language;
  DateTime? publishDate;
  Picture? picture;
  Publisher? publisher;
  int? volume;
  Meta? meta;
  Serie? serie;
  String? description;
  Identifier? identifier;
  int? pageCount;
  String? maturityRating;
  List<Tag>? tags;
  Epub? epub;
  GoogleBook? googleBook;
  bool? isFavorite;
  // List<dynamic> comments;

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);
  Map<String, dynamic> toJson() => _$BookToJson(this);
}
