import 'package:json_annotation/json_annotation.dart';

part 'Picture.g.dart';

@JsonSerializable()
class Picture {
  Picture({
    this.base,
    this.openGraph,
    this.original,
    this.color,
  });

  String? base;
  String? openGraph;
  String? original;
  String? color;

  factory Picture.fromJson(Map<String, dynamic> json) =>
      _$PictureFromJson(json);
  Map<String, dynamic> toJson() => _$PictureToJson(this);
}
