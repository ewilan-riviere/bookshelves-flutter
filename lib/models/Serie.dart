import 'package:bookshelves/models/Author.dart';
import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/models/Meta.dart';
import 'package:bookshelves/models/Picture.dart';
import 'package:bookshelves/models/Tag.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Serie.g.dart';

@JsonSerializable()
class Serie {
  Serie({
    this.title,
    this.slug,
    this.author,
    this.picture,
    this.meta,
    this.language,
    this.authors,
    this.count,
    this.description,
    this.link,
    this.tags,
    this.download,
    this.size,
    this.books,
    this.isFavorite,
    // this.comments = '',
  });

  String? title;
  String? slug;
  String? author;
  Picture? picture;
  Meta? meta;
  String? language;
  List<Author>? authors;
  int? count;
  String? description;
  String? link;
  List<Tag>? tags;
  String? download;
  String? size;
  @JsonKey(includeIfNull: true, defaultValue: [])
  List<Book>? books;
  bool? isFavorite;
  // List<dynamic> comments;

  factory Serie.fromJson(Map<String, dynamic> json) => _$SerieFromJson(json);
  Map<String, dynamic> toJson() => _$SerieToJson(this);
}
