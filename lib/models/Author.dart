// To parse this JSON data, do
//
//     final author = authorFromMap(jsonString);

import 'dart:convert';

import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/models/Meta.dart';
import 'package:bookshelves/models/Picture.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Author.g.dart';

@JsonSerializable()
class Author {
  Author({
    this.name,
    this.slug,
    this.meta,
    this.lastname,
    this.firstname,
    this.picture,
    this.count,
    this.description,
    this.link,
    this.size,
    this.download,
    this.series,
    this.books,
    this.isFavorite,
    // this.comments,
  });

  String? name;
  String? slug;
  Meta? meta;
  String? lastname;
  String? firstname;
  Picture? picture;
  int? count;
  String? description;
  String? link;
  String? size;
  String? download;
  List<Serie>? series;
  List<Book>? books;
  bool? isFavorite;
  // List<dynamic> comments;

  factory Author.fromJson(Map<String, dynamic> json) => _$AuthorFromJson(json);
  Map<String, dynamic> toJson() => _$AuthorToJson(this);

  static String getAuthorList(List<Author> authors) {
    var result = authors.map((author) => author.name).join(', ');

    return result;
  }
}
