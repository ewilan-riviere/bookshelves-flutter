import 'package:bookshelves/screens/about/about_screen.dart';
import 'package:bookshelves/screens/books-details/books_details_screen.dart';
import 'package:bookshelves/screens/home/home_screen.dart';
import 'package:bookshelves/screens/series/Series_screen.dart';
import 'package:bookshelves/screens/settings/settings_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static const String homeRoute = '/';
  static const String seriesRoute = '/series';
  static const String settingsRoute = '/settings';
  static const String aboutRoute = '/about';
  static const String bookRoute = '/book';

  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => HomeScreen(),
        );
      case seriesRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => SeriesScreen(),
        );
      case settingsRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => SettingsScreen(),
        );
      case aboutRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => AboutScreen(),
        );
      // case bookRoute:
      //   return MaterialPageRoute(
      //     settings: settings,
      //     builder: (_) => BooksDetailsScreen(),
      //   );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => HomeScreen(),
        );
    }
  }
}
