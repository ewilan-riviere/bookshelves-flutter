import 'package:bookshelves/providers/preferences_provider.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:flutter/material.dart';

class ThemeNotifier with ChangeNotifier {
  final darkTheme = ThemeData(
    primarySwatch: MaterialColor(primaryHexa, color),
    primaryColor: primary,
    brightness: Brightness.dark,
    backgroundColor: const Color(0xFF212121),
    accentColor: Colors.white,
    accentIconTheme: IconThemeData(color: Colors.black),
    dividerColor: Colors.black12,
    highlightColor: primary,
    scrollbarTheme: ScrollbarThemeData().copyWith(
      thumbColor: MaterialStateProperty.all(Colors.grey.shade900),
    ),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      },
    ),
  );

  final lightTheme = ThemeData(
    primarySwatch: MaterialColor(primaryHexa, color),
    primaryColor: primary,
    brightness: Brightness.light,
    backgroundColor: const Color(0xFFE5E5E5),
    accentColor: Colors.black,
    accentIconTheme: IconThemeData(color: Colors.white),
    dividerColor: Colors.grey.shade200,
    highlightColor: primary,
    scrollbarTheme: ScrollbarThemeData().copyWith(
      thumbColor: MaterialStateProperty.all(Colors.grey.shade500),
    ),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      },
    ),
  );

  var _themeData;
  ThemeData getTheme() => _themeData ?? darkTheme;

  ThemeNotifier() {
    PreferencesProvider.readData('themeMode').then((value) {
      String? themeMode = value;
      if (themeMode == 'light') {
        _themeData = lightTheme;
      } else {
        _themeData = darkTheme;
      }
      notifyListeners();
    });
  }

  void setDarkMode() async {
    _themeData = darkTheme;
    await PreferencesProvider.saveData('themeMode', 'dark');
    print(await PreferencesProvider.readData('themeMode'));
    notifyListeners();
  }

  void setLightMode() async {
    _themeData = lightTheme;
    await PreferencesProvider.saveData('themeMode', 'light');
    notifyListeners();
  }
}
