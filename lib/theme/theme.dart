import 'package:bookshelves/utils/helper.dart';
import 'package:flutter/material.dart';

// class BookshelvesTheme {
//   static final ThemeData lightTheme = _lightTheme();
//   static final ThemeData darkTheme = _darkTheme();

//   static ThemeData _lightTheme() {
//     final base = ThemeData.light();

//     return base.copyWith(
//       primaryColor: primary,
//       brightness: Brightness.light,
//       backgroundColor: const Color(0xFFE5E5E5),
//       accentColor: primary,
//       accentIconTheme: IconThemeData(color: Colors.white),
//       accentColorBrightness: Brightness.light,
//       primaryColorDark: Colors.grey.shade800,
//       primaryColorLight: Colors.amber,
//       primaryColorBrightness: Brightness.dark,
//       elevatedButtonTheme: ElevatedButtonThemeData(
//         style: ButtonStyle(
//           backgroundColor: MaterialStateProperty.all(primary),
//         ),
//       ),
//       buttonTheme: base.buttonTheme.copyWith(
//         buttonColor: Colors.yellow,
//         textTheme: ButtonTextTheme.primary,
//       ),
//       scaffoldBackgroundColor: Colors.white,
//       cardColor: Colors.white,
//       iconTheme: IconThemeData(color: Colors.white, opacity: 1.0),
//       primaryIconTheme: IconThemeData(color: Colors.black),
// scrollbarTheme: ScrollbarThemeData().copyWith(
//   thumbColor: MaterialStateProperty.all(Colors.grey[500]),
// ),
//       textTheme: base.textTheme.copyWith(
//         bodyText1: TextStyle(color: Colors.black),
//         bodyText2: TextStyle(color: Colors.black),
//         button: TextStyle(color: Colors.black),
//         caption: TextStyle(color: Colors.black),
//         headline1: TextStyle(color: Colors.black),
//         subtitle1: TextStyle(color: Colors.black),
//         subtitle2: TextStyle(color: Colors.black),
//       ),
// pageTransitionsTheme: PageTransitionsTheme(
//   builders: {
//     TargetPlatform.android: CupertinoPageTransitionsBuilder(),
//   },
// ),
//     );
//   }

//   static ThemeData _darkTheme() {
//     final base = ThemeData.dark();

//     return base.copyWith(
//       primaryColor: Colors.grey.shade900,
//       brightness: Brightness.dark,
//       backgroundColor: const Color(0xFF212121),
//       accentColor: Colors.white,
//       accentIconTheme: IconThemeData(color: Colors.black),
//       dividerColor: Colors.black12,
//       accentColorBrightness: Brightness.dark,
//       primaryColorDark: Colors.grey.shade800,
//       primaryColorLight: Colors.amber,
//       primaryColorBrightness: Brightness.dark,
//       // buttonTheme: base.buttonTheme.copyWith(
//       //   buttonColor: Colors.yellow,
//       //   textTheme: ButtonTextTheme.primary,
//       // ),
//       // scaffoldBackgroundColor: Colors.grey.shade800,
//       // cardColor: Colors.grey.shade100,
//       // scrollbarTheme: ScrollbarThemeData().copyWith(
//       //   thumbColor: MaterialStateProperty.all(Colors.grey[500]),
//       // ),
//       // textTheme: base.textTheme.copyWith(
//       //   bodyText1: TextStyle(color: Colors.white),
//       //   bodyText2: TextStyle(color: Colors.white),
//       //   button: TextStyle(color: Colors.white),
//       //   caption: TextStyle(color: Colors.white),
//       //   headline1: TextStyle(color: Colors.white),
//       //   subtitle1: TextStyle(color: Colors.white),
//       //   subtitle2: TextStyle(color: Colors.white),
//       // ),
//     );
//   }
// }
