import 'dart:convert';

import 'package:bookshelves/utils/constants.dart';
import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:package_info/package_info.dart';

class AboutScreen extends StatefulWidget {
  AboutScreen({Key? key, this.api}) : super(key: key);
  final String? api;

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  late final _scrollController;
  late PackageInfo packageInfo;
  late String license;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: 'About',
      widget: Scrollbar(
        controller: _scrollController,
        isAlwaysShown: true,
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: FutureBuilder(
            future: _getAboutData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return _aboutBody();
              } else {
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                    ],
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  List<Map<String, String>> _getListPackage(PackageInfo packageInfo) {
    var list = <Map<String, String>>[];

    var author = {'label': 'Author', 'value': 'Ewilan Rivière'};
    var package = {'label': 'Package', 'value': packageInfo.packageName};
    var version = {
      'label': 'Version',
      'value': 'v${packageInfo.version} (build ${packageInfo.buildNumber})'
    };
    list.add(author);
    list.add(package);
    list.add(version);

    return list;
  }

  Future<String> _getLicense() async {
    var data = await rootBundle.load('LICENSE');

    final buffer = data.buffer;
    var list = buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    var string = utf8.decode(list);

    return string;
  }

  Future<bool> _getAboutData() async {
    var success = false;
    var licenseStr = await _getLicense();
    var pckInfo = await PackageInfo.fromPlatform();
    setState(() {
      license = licenseStr;
      packageInfo = pckInfo;
    });
    success = true;

    return success;
  }

  Widget _aboutBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    Constants.title,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.handlee(fontSize: 32),
                  ),
                ),
                Image.asset(
                  'assets/images/icon.png',
                  height: 100.0,
                  alignment: Alignment.center,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 15.0, horizontal: 15.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: _getListPackage(packageInfo).length,
                    itemBuilder: (context, index) {
                      var value = _getListPackage(packageInfo)[index];
                      return Row(
                        children: [
                          Text(
                            '${value['label']!}: ',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(value['value']!),
                        ],
                      );
                    },
                  ),
                ),
                ExpansionTile(
                  childrenPadding: const EdgeInsets.all(10.0),
                  title: Text('Bookshelves License'),
                  children: [
                    Text(license),
                  ],
                ),
                OutlinedButton(
                  onPressed: () => showLicensePage(
                    context: context,
                    applicationIcon: Image.asset(
                      'assets/images/icon.png',
                      height: 50.0,
                    ),
                    applicationLegalese: 'BSD 2-Clause License',
                    applicationName: Constants.title,
                    applicationVersion: packageInfo.version,
                  ),
                  child: Text('Show all licenses of Bookshelves'),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

void showLicensePage({
  required BuildContext context,
  String? applicationName,
  String? applicationVersion,
  Widget? applicationIcon,
  String? applicationLegalese,
  bool useRootNavigator = false,
}) {
  Navigator.of(context, rootNavigator: useRootNavigator).push(
    MaterialPageRoute<void>(
      builder: (BuildContext context) => LicensePage(
        applicationName: applicationName,
        applicationVersion: applicationVersion,
        applicationIcon: applicationIcon,
        applicationLegalese: applicationLegalese,
      ),
    ),
  );
}
