import 'dart:convert';

import 'package:epub_viewer/epub_viewer.dart';
import 'package:flutter/material.dart';

class EpubReader extends StatefulWidget {
  EpubReader({Key? key, required this.path}) : super(key: key);
  final String path;

  @override
  _EpubReaderState createState() => _EpubReaderState();
}

class _EpubReaderState extends State<EpubReader> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Plugin example app'),
      ),
      body: Center(
        child: TextButton(
          onPressed: () async {
            EpubViewer.setConfig(
              themeColor: Theme.of(context).primaryColor,
              identifier: 'iosBook',
              scrollDirection: EpubScrollDirection.ALLDIRECTIONS,
              allowSharing: true,
              enableTts: true,
              nightMode: false,
            );
            EpubViewer.open(
              widget.path,
              lastLocation: EpubLocator.fromJson({
                'bookId': '2239',
                'href': '/OEBPS/ch06.xhtml',
                'created': 1539934158390,
                'locations': {'cfi': 'epubcfi(/0!/4/4[simple_book]/2/2/6)'}
              }),
            );
            // get current locator
            EpubViewer.locatorStream.listen((locator) {
              print('LOCATOR: ${EpubLocator.fromJson(jsonDecode(locator))}');
            });
          },
          child: Container(
            child: Text('open epub'),
          ),
        ),
      ),
    );
  }
}
