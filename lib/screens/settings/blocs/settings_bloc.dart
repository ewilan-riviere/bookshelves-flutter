import 'dart:convert';

import 'package:bookshelves/providers/preferences_provider.dart';
import 'package:flutter/material.dart';

class SettingsBloc {
  Future<List<String>> _apiList() async {
    var apiListJson = await PreferencesProvider.readData('apiList');
    var apiListDyn = jsonDecode(apiListJson!);
    var apiList = List<String>.from(apiListDyn);

    return apiList;
  }

  void _addToList(String newApi) async {
    // FocusScope.of(context).unfocus();
    // var api = _newApiController.text;
    // var _validURL = Uri.parse(newApi).isAbsolute;
    // if (_validURL) {
    //   var list = await _apiList();
    //   if (!list.contains(newApi)) {
    //     list.add(newApi);
    //   } else {
    //     ScaffoldMessenger.of(context).hideCurrentSnackBar();
    //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //       content: Text('Already available in API list'),
    //       backgroundColor: Colors.red,
    //     ));
    //     return;
    //   }
    //   var prefs = await SharedPreferences.getInstance();
    //   await prefs.setString('apiList', jsonEncode(list));
    //   setState(() {
    //     _futureApiList = _apiList();
    //   });
    //   SettingsApiSelect();
    //   _validate(api);
    // } else {
    //   ScaffoldMessenger.of(context).hideCurrentSnackBar();
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     content: Text('This API is not a valid URL'),
    //     backgroundColor: Colors.red,
    //   ));
    // }
  }

  void _validate(value) async {
    // var prefs = await SharedPreferences.getInstance();
    // await prefs.setString('api', value);
    // await prefs.setBool('apiIsSet', true);

    // Navigator.pop(context);
    // SnackbarProvider.show(
    //   context: context,
    //   text: 'Validate new source: $value',
    //   type: SnackbarType.success,
    // );
    // await Navigator.of(context).pushAndRemoveUntil(
    //   MaterialPageRoute(
    //     builder: (context) => Bookshelves(),
    //   ),
    //   (route) => false,
    // );
  }
}
