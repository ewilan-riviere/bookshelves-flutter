import 'package:bookshelves/utils/constants.dart';
import 'package:bookshelves/widgets/input/app_radio_list.dart';
import 'package:flutter/material.dart';

class SettingsApiSelect extends StatefulWidget {
  final String? api;
  final Map<String, String>? apiList;
  SettingsApiSelect({Key? key, this.api, this.apiList}) : super(key: key);

  @override
  _SettingsApiSelectState createState() => _SettingsApiSelectState();
}

class _SettingsApiSelectState extends State<SettingsApiSelect> {
  final ScrollController _scrollController = ScrollController();
  // https://framabook.org
  final _newApiController = TextEditingController(text: '');
  var _apiValue;
  late Future<List<String>> _futureApiList;

  @override
  void initState() {
    super.initState();
    // _futureApiList = _apiList();
    _futureApiList = setApiList();
  }

  Future<List<String>> setApiList() async {
    return Constants.apiList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.8,
      child: Column(
        children: [
          SizedBox(
            height: 50,
            child: Center(
              child: Text(
                'Available sources',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0),
              ),
            ),
          ),
          const Divider(
            thickness: 1,
            height: 0,
          ),
          FutureBuilder<List<String>>(
            future: _futureApiList,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var apiList = snapshot.data;
                return Expanded(
                  child: Scrollbar(
                    isAlwaysShown: true,
                    controller: _scrollController,
                    child: AppRadioList(
                      scrollController: _scrollController,
                      list: apiList!,
                      currentItem: _apiValue,
                      // itemCallBack: (value) => _validate(value),
                      itemCallBack: (value) {},
                    ),
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
          // Expanded(
          //   child: Scrollbar(
          //     controller: _scrollController,
          //     isAlwaysShown: true,
          //     child: SingleChildScrollView(
          //       controller: _scrollController,
          //       child: FutureBuilder<List<String>>(
          //         future: _futureApiList,
          //         builder: (context, snapshot) {
          //           if (snapshot.hasData) {
          //             var apiList = snapshot.data;
          //             return AppRadioList(
          //               list: apiList!,
          //               currentItem: _apiValue,
          //               // itemCallBack: (value) => _validate(value),
          //               itemCallBack: (value) {},
          //             );
          //           } else {
          //             return Container();
          //           }
          //         },
          //       ),
          //     ),
          //   ),
          // ),
          // Expanded(
          //   child: Scrollbar(
          //     controller: _scrollController,
          //     isAlwaysShown: true,
          //     child: SingleChildScrollView(
          //       controller: _scrollController,
          // child: FutureBuilder<List<String>>(
          //   future: _futureApiList,
          //   builder: (context, snapshot) {
          //     if (snapshot.hasData) {
          //       var apiList = snapshot.data;
          //       return AppRadioList(
          //         list: apiList!,
          //         currentItem: _apiValue,
          //         // itemCallBack: (value) => _validate(value),
          //         itemCallBack: (value) {},
          //       );
          //     } else {
          //       return Container();
          //     }
          //   },
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
