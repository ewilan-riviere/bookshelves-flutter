import 'dart:convert';

import 'package:bookshelves/providers/preferences_provider.dart';
import 'package:bookshelves/screens/settings/widgets/settings_api_select.dart';
import 'package:bookshelves/theme/theme_manager.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:bookshelves/widgets/input/app_input_text_material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

final GlobalKey<_SettingsScreenState> settingsKey = GlobalKey();

class SettingsScreen extends StatefulWidget {
  SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  var _darkTheme = false;
  var _api = Constants.apiUrlDefault;
  var _license;

  @override
  void initState() {
    super.initState();
    settings();
  }

  Future<void> settings() async {
    var api = await PreferencesProvider.readData('api');
    var themeMode = await PreferencesProvider.readData('themeMode');

    setState(() {
      _api = api ?? Constants.apiUrlDefault;
      if (themeMode == 'dark') {
        _darkTheme = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: FutureBuilder(
        future: settings(),
        builder: (context, snapshot) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 5.0,
            ),
            child: Column(
              children: [
                _generalSection(_api),
                _aboutSection(),
              ],
            ),
          );
        },
      ),
    );
  }

  void _setMode(ThemeNotifier themeNotifier) {
    if (_darkTheme) {
      themeNotifier.setDarkMode();
    } else {
      themeNotifier.setLightMode();
    }
  }

  Widget _generalSection(String? api) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 10.0, bottom: 5.0, left: 6.0),
            child: Text(
              'General',
              style: TextStyle(
                fontSize: 16.0,
              ),
              textAlign: TextAlign.start,
            ),
          ),
        ),
        tileSource(),
        tileMode(),
      ],
    );
  }

  Widget tileMode() {
    return Consumer<ThemeNotifier>(
      builder: (context, theme, _) => Card(
        child: SwitchListTile(
          subtitle: Text(
            'Change theme mode',
            style: TextStyle(
              color: Colors.grey.shade500,
            ),
          ),
          title: Text('Dark mode'),
          secondary: Icon(Icons.palette),
          value: _darkTheme,
          activeColor: primary,
          inactiveTrackColor: Colors.grey,
          onChanged: (bool value) {
            setState(() {
              _darkTheme = value;
              _setMode(theme);
            });
          },
        ),
      ),
    );
  }

  Widget tileSource() {
    return Card(
      child: ListTile(
        leading: Icon(Icons.tune),
        title: Text('Source'),
        subtitle: Text(_api),
        onTap: () {
          showModalBottomSheet<void>(
            context: context,
            builder: (context) {
              return SettingsApiSelect();
            },
          );
        },
      ),
    );
  }

  Widget _aboutSection() {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 10.0, bottom: 5.0, left: 6.0),
            child: Text(
              'Misc',
              style: TextStyle(
                fontSize: 16.0,
              ),
              textAlign: TextAlign.start,
            ),
          ),
        ),
        tileAbout()
      ],
    );
  }

  Widget addNewSource() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 20.0,
      ),
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // Expanded(
            //   child: AppInputTextMaterial(
            //     // controller: _newApiController,
            //     textInputType: TextInputType.url,
            //     // onEditingCompleteMethod: () =>
            //     //     _validate(_newApiController.text),
            //     autofocus: false,
            //     label: 'Add new API',
            //     hint: 'https://www.mydomain.com',
            //     borderRadius: BorderRadius.zero,
            //   ),
            // ),
            // ElevatedButton(
            //   style: ButtonStyle(
            //     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            //       RoundedRectangleBorder(
            //         borderRadius: BorderRadius.zero,
            //       ),
            //     ),
            //   ),
            //   onPressed: () => _addToList(),
            //   child: Text('Add'),
            // )
          ],
        ),
      ),
    );
  }

  Widget tileAbout() {
    return Card(
      child: ListTile(
        leading: Icon(Icons.info),
        title: Text('About'),
        subtitle: Text('The Bookshelves team'),
        onTap: () async {
          var packageInfo = await PackageInfo.fromPlatform();
          showAboutDialog(
              context: context,
              applicationIcon: Image.asset(
                'assets/images/icon.png',
                height: 50.0,
              ),
              applicationLegalese: 'BSD 2-Clause License, Ewilan Rivière.',
              applicationName: packageInfo.appName,
              applicationVersion:
                  'v${packageInfo.version} (build ${packageInfo.buildNumber})',
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'Bookshelves is an OpenSource Flutter application. You can know more on ',
                          style: TextStyle(color: Colors.black),
                        ),
                        TextSpan(
                          text: 'Bookshelves Wiki',
                          style: TextStyle(color: Colors.blue),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              launch(
                                'https://bookshelves.ink/api/wiki',
                              );
                            },
                        ),
                        TextSpan(
                          text: '.',
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  // child: Text(
                  //     ''),
                ),
              ]);
        },
      ),
    );
  }
}
