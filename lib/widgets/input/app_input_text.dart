import 'package:bookshelves/widgets/input/text_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppInputText extends StatefulWidget {
  final String label;
  final TextInputType textInputType;
  final TextEditingController controller;
  final List<TextInputFormatter> formatters;
  final bool obscureText;
  final VoidCallback onEditingCompleteMethod;
  final TextInputAction textInputAction;
  final bool autofocus;
  final String hint;

  const AppInputText({
    Key? key,
    this.label = '',
    this.textInputType = TextInputType.text,
    required this.controller,
    this.formatters = const <TextInputFormatter>[],
    this.obscureText = false,
    this.onEditingCompleteMethod = onEditingCompleteMethodDefault,
    this.textInputAction = TextInputAction.done,
    this.autofocus = false,
    this.hint = 'Hint here',
  }) : super(key: key);

  @override
  _AppInputTextState createState() => _AppInputTextState();

  static void onEditingCompleteMethodDefault() {
    print('No onEditingCompleteMethod');
  }
}

class _AppInputTextState extends State<AppInputText> {
  late String label;
  late TextInputType textInputType;
  late TextEditingController controller;
  late List<TextInputFormatter> formatters;
  late bool obscureText;
  late bool obscureTextBase;
  late VoidCallback onEditingCompleteMethod;
  late TextInputAction textInputAction;
  late bool autofocus;

  @override
  void initState() {
    super.initState();
    label = widget.label;
    textInputType = widget.textInputType;
    controller = widget.controller;
    formatters = widget.formatters;
    obscureText = widget.obscureText;
    obscureTextBase = widget.obscureText;
    onEditingCompleteMethod = widget.onEditingCompleteMethod;
    textInputAction = widget.textInputAction;
    autofocus = widget.autofocus;
  }

  // Toggles the password show status
  void _toggle() {
    setState(() {
      obscureText = !obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 5),
        child: Stack(
          children: [
            Card(
              elevation: 1,
              child: SizedBox(
                height: 52,
                child: TextFormField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(
                      top: 10,
                      right: 20,
                      bottom: 10,
                      left: 15,
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: widget.hint,
                  ),
                  inputFormatters: [BasicTextFormatter()],
                  keyboardType: textInputType,
                  obscureText: obscureText,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Ce champs est obligatoire';
                    }
                    return null;
                  },
                  autofocus: autofocus,
                  controller: controller,
                  onEditingComplete: onEditingCompleteMethod,
                  textInputAction: textInputAction,
                ),
              ),
              margin: EdgeInsets.zero,
            ),
            Positioned(
              top: 0,
              bottom: 0,
              right: obscureTextBase ? 50 : 10,
              child: Container(
                color: Colors.transparent,
                child: Center(
                  child: Text(
                    label,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              bottom: 0,
              right: 0,
              child: obscureTextBase
                  ? IconButton(
                      icon: Icon(
                        obscureText ? Icons.visibility : Icons.visibility_off,
                        color: Theme.of(context).primaryColor,
                      ),
                      onPressed: _toggle,
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}
