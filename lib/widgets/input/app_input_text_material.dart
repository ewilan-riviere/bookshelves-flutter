import 'package:bookshelves/widgets/input/text_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppInputTextMaterial extends StatefulWidget {
  final String label;
  final TextInputType textInputType;
  final TextEditingController controller;
  final List<TextInputFormatter> formatters;
  final bool obscureText;
  final VoidCallback onEditingCompleteMethod;
  final TextInputAction textInputAction;
  final bool autofocus;
  final String hint;
  final BorderRadius? borderRadius;

  const AppInputTextMaterial({
    Key? key,
    this.label = '',
    this.textInputType = TextInputType.text,
    required this.controller,
    this.formatters = const <TextInputFormatter>[],
    this.obscureText = false,
    this.onEditingCompleteMethod = onEditingCompleteMethodDefault,
    this.textInputAction = TextInputAction.done,
    this.autofocus = false,
    this.hint = 'Hint here',
    this.borderRadius,
  }) : super(key: key);

  @override
  _AppInputTextMaterialState createState() => _AppInputTextMaterialState();

  static void onEditingCompleteMethodDefault() {
    print('No onEditingCompleteMethod');
  }
}

class _AppInputTextMaterialState extends State<AppInputTextMaterial> {
  late String label;
  late TextInputType textInputType;
  late TextEditingController controller;
  late List<TextInputFormatter> formatters;
  late bool obscureText;
  late bool obscureTextBase;
  late VoidCallback onEditingCompleteMethod;
  late TextInputAction textInputAction;
  late bool autofocus;

  @override
  void initState() {
    super.initState();
    label = widget.label;
    textInputType = widget.textInputType;
    controller = widget.controller;
    formatters = widget.formatters;
    obscureText = widget.obscureText;
    obscureTextBase = widget.obscureText;
    onEditingCompleteMethod = widget.onEditingCompleteMethod;
    textInputAction = widget.textInputAction;
    autofocus = widget.autofocus;
  }

  // Toggles the password show status
  void _toggle() {
    setState(() {
      obscureText = !obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      child: TextFormField(
        inputFormatters: [BasicTextFormatter()],
        keyboardType: textInputType,
        obscureText: obscureText,
        validator: (value) {
          if (value!.isEmpty) {
            return 'Ce champs est obligatoire';
          }
          return null;
        },
        autofocus: autofocus,
        controller: controller,
        onEditingComplete: onEditingCompleteMethod,
        textInputAction: textInputAction,
        decoration: InputDecoration(
          hintText: widget.hint,
          // prefixIcon: Icon(Icons.link),
          labelText: label,
          // errorText: 'Error message',
          border: OutlineInputBorder(
            borderRadius: widget.borderRadius ??
                const BorderRadius.all(Radius.circular(4.0)),
          ),
          // suffixIcon: Icon(
          //   Icons.error,
          //   color: Colors.red,
          // ),
        ),
      ),
    );
  }
}
