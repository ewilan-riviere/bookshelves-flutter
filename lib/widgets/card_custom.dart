import 'package:flutter/material.dart';

class CardCustom extends StatelessWidget {
  const CardCustom(
      {Key? key,
      this.leading,
      this.title,
      this.subtitle,
      this.text,
      this.firstButton,
      this.secondButton})
      : super(key: key);
  final Widget? leading;
  final Widget? title;
  final Widget? subtitle;
  final String? text;
  final TextButton? firstButton;
  final TextButton? secondButton;

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            leading: leading ??
                Icon(
                  Icons.info,
                  size: 30.0,
                ),
            title: title ?? const Text('Title'),
            subtitle: subtitle ??
                Text(
                  'Subtitle',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
          ),
          if (text != null)
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                text!,
                style: TextStyle(color: Colors.black.withOpacity(0.6)),
                textAlign: TextAlign.justify,
              ),
            ),
          ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [firstButton ?? Text(''), secondButton ?? Text('')],
          ),
        ],
      ),
    );
  }
}
