import 'dart:ui';
import 'package:flutter/material.dart';

class BlurryDialog extends StatelessWidget {
  final String title;
  final String content;
  final bool blur;
  final TextButton? firstButton;
  final TextButton? secondButton;

  BlurryDialog(
      {required this.title,
      required this.content,
      this.blur = true,
      this.firstButton,
      this.secondButton});
  final TextStyle textStyle = TextStyle(color: Colors.black);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter:
          blur ? ImageFilter.blur(sigmaX: 6, sigmaY: 6) : ImageFilter.blur(),
      child: AlertDialog(
        title: Text(
          title,
          style: textStyle,
        ),
        content: Text(
          content,
          style: textStyle,
        ),
        actions: <Widget>[
          firstButton ?? Text(''),
          secondButton ?? Text(''),
        ],
      ),
    );
  }
}
