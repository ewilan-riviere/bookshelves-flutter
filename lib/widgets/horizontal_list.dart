import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var edge = 120.0;
    var padding = edge / 10.0;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: Container(
          color: Colors.red,
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                for (int i = 0; i < 10; i++)
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(padding),
                        child: ClipRRect(
                          borderRadius:
                              BorderRadius.all(Radius.circular(edge * .2)),
                          child: Container(
                            width: edge,
                            height: edge,
                            color: Colors.blue,
                            child: Image.network(
                                'http://via.placeholder.com/${edge.round()}x${edge.round()}',
                                fit: BoxFit.fitHeight),
                          ),
                        ),
                      ),
                      Container(
                        width: edge + padding,
                        padding: EdgeInsets.only(left: padding),
                        child: Text(
                          'foo app bar baz app apk',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: padding),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('4.2'),
                            Icon(
                              Icons.star,
                              size: 16,
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
