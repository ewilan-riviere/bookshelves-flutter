import 'package:flutter/material.dart';

class HorizontalListSnap extends StatefulWidget {
  HorizontalListSnap({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HorizontalListSnapState createState() => _HorizontalListSnapState();
}

class _HorizontalListSnapState extends State<HorizontalListSnap> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: SizedBox(
          height: 200, // card height
          child: PageView.builder(
            itemCount: 20,
            controller: PageController(viewportFraction: 0.7),
            onPageChanged: (int index) => setState(() => _index = index),
            itemBuilder: (_, i) {
              return Transform.scale(
                scale: i == _index ? 1 : 1,
                child: Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: Text(
                      'Card ${i + 1}',
                      style: TextStyle(fontSize: 32),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
