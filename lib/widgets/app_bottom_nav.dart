import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/theme/theme.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:flutter/material.dart';

class AppBottomNav extends StatefulWidget {
  AppBottomNav({Key? key}) : super(key: key);

  @override
  _AppBottomNavState createState() => _AppBottomNavState();
}

class _AppBottomNavState extends State<AppBottomNav> {
  late int _currentIndex = 0;
  late final _children = _list();

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
      backgroundColor: primary,
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white.withOpacity(.60),
      selectedFontSize: 14,
      unselectedFontSize: 14,
      onTap: (value) {
        print(value);
        print(_children[value].route);
        if (ModalRoute.of(context)!.settings.name != _children[value].route) {
          Navigator.of(context).pushNamed(_children[value].route);
        }
        setState(() {
          _currentIndex = value;
        });
      },
      items: _children,
    );
  }

  List<NavigationBarItem> _list() {
    return [
      NavigationBarItem(
        label: 'Home',
        icon: Icon(
          Icons.home,
        ),
        route: AppRouter.homeRoute,
      ),
      NavigationBarItem(
        label: 'Series',
        icon: Icon(
          Icons.collections,
        ),
        route: AppRouter.seriesRoute,
      ),
    ];
  }
}

class NavigationBarItem extends BottomNavigationBarItem {
  const NavigationBarItem(
      {required this.label, required this.icon, required this.route})
      : super(icon: icon, label: label);

  @override
  final String label;
  final Icon icon;
  final String route;
}
