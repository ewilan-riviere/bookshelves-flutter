import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  final String? title;
  final Map<String, String> items;
  final ValueChanged<String> itemCallBack;
  final String? currentItem;
  final String hintText;

  DropdownWidget({
    this.title,
    required this.items,
    required this.itemCallBack,
    this.currentItem,
    required this.hintText,
  });

  @override
  State<StatefulWidget> createState() => _DropdownState(currentItem);
}

class _DropdownState extends State<DropdownWidget> {
  List<DropdownMenuItem<String>> dropDownItems = [];
  String? currentItem;
  AppTheme? appTheme;

  _DropdownState(this.currentItem);

  @override
  void initState() {
    super.initState();

    dropDownItems = widget.items
        .map((description, value) {
          return MapEntry(
            description,
            DropdownMenuItem<String>(
              value: value,
              child: Text(description),
            ),
          );
        })
        .values
        .toList();
  }

  @override
  void didUpdateWidget(DropdownWidget oldWidget) {
    if (currentItem != widget.currentItem) {
      setState(() {
        currentItem = widget.currentItem!;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    // appTheme = Theme.of(context).brightness as AppTheme?;

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          if (widget.title != null)
            Container(
              margin: EdgeInsets.only(left: 6),
              child: Text(
                widget.title!,
                // style: appTheme.activityAddPageTextStyle,
              ),
            ),
          Card(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 3, horizontal: 15),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  // icon: appTheme.activityAddPageDownArrowSVG,
                  icon: Icon(Icons.arrow_drop_down_rounded),
                  value: currentItem,
                  isExpanded: true,
                  items: dropDownItems,
                  onChanged: (selectedItem) => setState(() {
                    currentItem = selectedItem as String;
                    widget.itemCallBack(currentItem!);
                  }),
                  hint: Container(
                    child: Text(
                      widget.hintText,
                      // style: appTheme.hintStyle,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AppTheme {}
