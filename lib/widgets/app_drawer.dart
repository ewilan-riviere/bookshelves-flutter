import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/widgets/blurry_dialog.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppDrawer extends StatelessWidget {
  final futurePackage = PackageInfo.fromPlatform();

  Future<String?> _getApi() async {
    var prefs = await SharedPreferences.getInstance();
    return await prefs.getString('api');
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: FutureBuilder(
        future: _getApi(),
        builder: (context, snapshot) {
          var apiExist = false;
          if (snapshot.hasData) {
            apiExist = true;
            return _buildListView(context, apiExist);
          } else {
            return _buildListView(context, apiExist);
          }
        },
      ),
    );
  }

  ListView _buildListView(BuildContext context, bool apiExist) {
    var list = <Widget>[];
    var listDefault = <Widget>[
      _createHeader(),
      _createDrawerItem(
        icon: Icons.home,
        text: 'Home',
        onTap: () {
          if (ModalRoute.of(context)!.settings.name != AppRouter.homeRoute) {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed(AppRouter.homeRoute);
          } else {
            Navigator.of(context).pop();
          }
        },
      ),
    ];
    var listApi = <Widget>[
      _createDrawerItem(
        icon: Icons.collections_bookmark_rounded,
        text: 'Series',
        onTap: () {
          if (ModalRoute.of(context)!.settings.name != AppRouter.seriesRoute) {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed(AppRouter.seriesRoute);
          } else {
            Navigator.of(context).pop();
          }
        },
      ),
    ];
    var listEnd = <Widget>[
      Divider(),
      _createDrawerItem(
        icon: Icons.settings,
        text: 'Settings',
        onTap: () {
          if (ModalRoute.of(context)!.settings.name !=
              AppRouter.settingsRoute) {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed(AppRouter.settingsRoute);
          } else {
            Navigator.of(context).pop();
          }
        },
      ),
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: FutureBuilder<PackageInfo>(
          future: futurePackage,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var package = snapshot.data;
              return TextButton(
                child: Text(
                  'v${package!.version}',
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(AppRouter.aboutRoute);
                },
              );
            } else {
              return Text('');
            }
          },
        ),
      )
    ];
    if (apiExist) {
      list.addAll(listDefault);
      list.addAll(listApi);
      list.addAll(listEnd);
    } else {
      list.addAll(listDefault);
      list.addAll(listEnd);
    }

    return ListView(
      padding: EdgeInsets.zero,
      children: list,
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/images/icon.jpg'),
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.4),
            BlendMode.dstATop,
          ),
        ),
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text(
              'Bookshelves',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createDrawerItem(
      {required IconData icon,
      required String text,
      GestureTapCallback? onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
