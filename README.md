# Bookshelves · Mobile

[![flutter](https://img.shields.io/static/v1?label=Flutter&message=v2.0&color=02569B&style=flat-square&logo=flutter&logoColor=ffffff)](https://flutter.dev)
[![dart](https://img.shields.io/static/v1?label=Dart&message=v2.12&color=0175C2&style=flat-square&logo=dart&logoColor=ffffff)](https://dart.dev)
[![dart](https://img.shields.io/static/v1?label=Dart&message=Null%20Safety&color=0175C2&style=flat-square&logo=dart&logoColor=ffffff)](https://dart.dev/null-safety/migration-guide)

>Mobile application, designed to be connected to Bookshelves API with [**bookshelves-back**](https://gitlab.com/ewilan-riviere/bookshelves-back). [Android Studio](https://developer.android.com/studio) is strongly recommanded to get Android emulators but you can use a real device to test application, it's works for iOS too on OSX.

## Setup

When you have install `flutter`

```bash
flutter pub get
```

```bash
flutter run
```

## Shortcuts

```bash
emmet stateful widget
Ctrl + . on widget
```

- <https://androidride.com/flutter-create-new-project-command-line-options/>
- <https://medium.com/flutter-community/flutter-and-the-command-line-a-love-story-a3648ef2411>
- <https://medium.com/nonstopio/flutter-best-practices-c3db1c3cd694>

## Design

- [**Library-App-Design**](https://dribbble.com/shots/10865368-Library-App-Design)
- [**E-Book-Mobile-App-UX-UI**](https://dribbble.com/shots/12214786-E-Book-Mobile-App-UX-UI)
- [**Library-App**](https://dribbble.com/shots/11281638-Library-App)

### Components

- [**material.io**](https://material.io/components?platform=flutter)
- [**argon**](https://www.creative-tim.com/learning-lab/flutter/overview/argon)
- [**flutter.dev**](https://flutter.dev/docs/development/ui/widgets/material)

```dart
Card(
    semanticContainer: true,
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Image.network(
    'https://placeimg.com/640/480/any',
    fit: BoxFit.fill,
    ),
    shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(10.0),
    ),
    elevation: 5,
    margin: EdgeInsets.all(10),
)
```

## Usage

- JsonSerializable
  - [**medium.com**](https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9)
  - [**developer.school**](https://developer.school/flutter-using-json_serializable-to-serialise-dart-classes/)
  - [**raywenderlich.com**](https://www.raywenderlich.com/books/flutter-apprentice/v1.0.ea2/chapters/11-serialization-with-json)
  - [**flutter-explained.dev**](https://flutter-explained.dev/blog/flutter-json-serialization/)
    - [**built_value**](https://pub.dev/packages/built_value)
    - [**built_collection**](https://pub.dev/packages/built_collection)
- Floor
  - [**floor**](https://pub.dev/packages/floor)
  - [**petercoding.com**](https://petercoding.com/flutter/2021/04/01/using-floor-plugin-in-flutter/)
- File
  - [**Use Android intent to open another app**](https://stackoverflow.com/questions/67064178/how-to-open-a-file-with-a-custom-file-extension-from-another-app-with-my-flutter)
    - [**Android: open native file manager**](https://stackoverflow.com/questions/11720032/open-default-file-manager-in-android)
    - [**Flutter way to open file manager**](https://stackoverflow.com/questions/54422880/how-to-open-native-file-explorer-to-locate-a-directory-in-flutter)
    - [**Create a button to open another app in Flutter**](https://stackoverflow.com/questions/58839952/create-a-button-to-open-another-app-in-flutter?rq=1)
- [**Open audio file from file manager with Flutter app**](https://stackoverflow.com/questions/59364498/how-to-open-audio-file-from-file-manager-using-flutter-app)

## Help

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
