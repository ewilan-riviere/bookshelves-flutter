.SILENT: run clean clean-ios aab apk apk-split ipa ios build

.PHONY: build clean

build: 
	./bin/build.sh $(type)

prerequisites: do_script

target: prerequisites 

get: 
	flutter pub get

update: 
	flutter pub update

run: get
	flutter run

clean:
	flutter clean
	flutter pub get

clean-ios:
	flutter clean
	rm -Rf ios/Pods
	rm -Rf ios/.symlinks
	rm -Rf ios:Flutter/Flutter.framework
	rm -Rf ios/Flutter/Flutter.podspec
	rm ios/Podfile.lock
	flutter pub get 
	cd ios; \
	pod update; \
	cd ../; \

aab: clean
	flutter build appbundle --release
	echo "AAB generated!"
	cp build/app/outputs/bundle/release/app-release.aab ./
	echo "app-release.aab is available at repository root!"

apk: clean
	flutter build apk
	echo "APK generated!"
	cp build/app/outputs/flutter-apk/app-release.apk ./
	echo "app-release.apk is available at repository root!"

apk-split: clean
	flutter build apk --split-per-abi
	echo "APK generated!"
	cp build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk ./
	echo "app-armeabi-v7a-release.apk is available at repository root!"

ipa: clean-ios
	flutter build ipa

# Create iOS application bundle, iOS archive bundle
ios: clean-ios
	flutter build ios
	make -s flutter build ipa

json:
	flutter pub run build_runner watch --delete-conflicting-outputs

icon:
	flutter pub run flutter_launcher_icons:main

splash:
	flutter pub run flutter_native_splash:create
